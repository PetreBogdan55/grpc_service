// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: Protos/person.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Tema1CNAClient {
  public static partial class GetPersonService
  {
    static readonly string __ServiceName = "personservice.GetPersonService";

    static void __Helper_SerializeMessage(global::Google.Protobuf.IMessage message, grpc::SerializationContext context)
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (message is global::Google.Protobuf.IBufferMessage)
      {
        context.SetPayloadLength(message.CalculateSize());
        global::Google.Protobuf.MessageExtensions.WriteTo(message, context.GetBufferWriter());
        context.Complete();
        return;
      }
      #endif
      context.Complete(global::Google.Protobuf.MessageExtensions.ToByteArray(message));
    }

    static class __Helper_MessageCache<T>
    {
      public static readonly bool IsBufferMessage = global::System.Reflection.IntrospectionExtensions.GetTypeInfo(typeof(global::Google.Protobuf.IBufferMessage)).IsAssignableFrom(typeof(T));
    }

    static T __Helper_DeserializeMessage<T>(grpc::DeserializationContext context, global::Google.Protobuf.MessageParser<T> parser) where T : global::Google.Protobuf.IMessage<T>
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (__Helper_MessageCache<T>.IsBufferMessage)
      {
        return parser.ParseFrom(context.PayloadAsReadOnlySequence());
      }
      #endif
      return parser.ParseFrom(context.PayloadAsNewBuffer());
    }

    static readonly grpc::Marshaller<global::Tema1CNAClient.Person> __Marshaller_personservice_Person = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Tema1CNAClient.Person.Parser));
    static readonly grpc::Marshaller<global::Tema1CNAClient.PersonData> __Marshaller_personservice_PersonData = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Tema1CNAClient.PersonData.Parser));

    static readonly grpc::Method<global::Tema1CNAClient.Person, global::Tema1CNAClient.PersonData> __Method_GetPersonData = new grpc::Method<global::Tema1CNAClient.Person, global::Tema1CNAClient.PersonData>(
        grpc::MethodType.Unary,
        __ServiceName,
        "GetPersonData",
        __Marshaller_personservice_Person,
        __Marshaller_personservice_PersonData);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Tema1CNAClient.PersonReflection.Descriptor.Services[0]; }
    }

    /// <summary>Client for GetPersonService</summary>
    public partial class GetPersonServiceClient : grpc::ClientBase<GetPersonServiceClient>
    {
      /// <summary>Creates a new client for GetPersonService</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public GetPersonServiceClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for GetPersonService that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public GetPersonServiceClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected GetPersonServiceClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected GetPersonServiceClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Tema1CNAClient.PersonData GetPersonData(global::Tema1CNAClient.Person request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetPersonData(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Tema1CNAClient.PersonData GetPersonData(global::Tema1CNAClient.Person request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_GetPersonData, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Tema1CNAClient.PersonData> GetPersonDataAsync(global::Tema1CNAClient.Person request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetPersonDataAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Tema1CNAClient.PersonData> GetPersonDataAsync(global::Tema1CNAClient.Person request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_GetPersonData, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override GetPersonServiceClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new GetPersonServiceClient(configuration);
      }
    }

  }
}
#endregion

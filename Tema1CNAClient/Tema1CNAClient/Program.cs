﻿using Grpc.Core;
using Grpc.Net.Client;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace Tema1CNAClient
{
    class Program
    {
        static void Main(string[] args)
        {

            var channel = GrpcChannel.ForAddress("https://localhost:5001");

            var client = new GetPersonService.GetPersonServiceClient(channel);

            char option =' ';
            do
            {
                try
                {

                    Console.WriteLine("1 - Trimite date catre server");
                    Console.WriteLine("2 - Iesire aplicatie");
                    option = Console.ReadKey().KeyChar;
                    Console.Clear();
                    switch (option)
                    {
                        case '1':
                            {
                                Console.WriteLine("\nIntroduceti numele clientului:");
                                var name = Console.ReadLine();
                                Console.WriteLine("\nIntroduceti CNP-ul clientului:");
                                var cnp = Console.ReadLine();

                                var data = client.GetPersonData(new Person { Name = name, Cnp = cnp });

                                if (data.Age != 0)
                                {
                                    Console.WriteLine("\nClientul cu numele " + name + " are varsta de " + data.Age + " ani si are sex " + data.Gender.ToString());
                                }
                                break;
                            }

                        case '2':
                            {
                                Console.WriteLine("\nClientul a fost inchis!");
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("\nOptiune invalida!");
                                break;
                            }
                    }
                }
                catch (RpcException e)
                {
                    Console.WriteLine(e.Status.Detail);
                }
            }while (option != 2);

            
            channel.ShutdownAsync();
        }
    }
}

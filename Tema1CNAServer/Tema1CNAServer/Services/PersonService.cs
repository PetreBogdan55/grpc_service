﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace Tema1CNAServer.Services
{
    public class PersonService : GetPersonService.GetPersonServiceBase
    {


        public PersonData.Types.Gender GetPersonGender(string cnp)
        {
            List<char> maleGender = new List<char>(new char[] { '1' , '3', '5', '7'});
            if (cnp[0] == '0') return PersonData.Types.Gender.Invalid;
            if (cnp[0] == '9') return PersonData.Types.Gender.Invalid;
            if (maleGender.Contains(cnp[0])) return PersonData.Types.Gender.Masculin;
            return PersonData.Types.Gender.Feminin;
        }

        public void VerifyRequestName(string name)
        {
            if (name == null) 
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Numele este invalid!"));
            if (name == String.Empty)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Numele este invalid!"));
            if(!Regex.IsMatch(name, @"^[a-zA-Z]+$"))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Numele contine cifre si/sau caractere speciale!"));

        }

        public int GetPersonAge(string cnp)
        {
            if(cnp.Length!=13)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "CNP-ul nu contine 13 cifre!"));
            if (!Regex.IsMatch(cnp, @"^[0-9]+$"))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "CNP-ul nu contine doar cifre!"));

            System.Text.StringBuilder yearOfBirth = new System.Text.StringBuilder();
            System.Text.StringBuilder monthOfBirth = new System.Text.StringBuilder();
            System.Text.StringBuilder dayOfBirth = new System.Text.StringBuilder();
            switch (cnp[0])
            {
                case '1':
                case '2':
                case '7':
                case '8':
                    yearOfBirth.Append("19");
                    break;
                case '3':
                case '4':
                    yearOfBirth.Append("18");
                    break;
                case '5':
                case '6':
                    yearOfBirth.Append("20");
                    break;
                case '0':
                    throw new RpcException(new Status(StatusCode.InvalidArgument, "CNP-ul nu poate incepe cu cifra 0!"));
                case '9':
                    throw new RpcException(new Status(StatusCode.InvalidArgument, "CNP-ul nu poate incepe cu cifra 9!"));

            }
            yearOfBirth.Append(cnp[1]).Append(cnp[2]);
            monthOfBirth.Append(cnp[3]).Append(cnp[4]);
            dayOfBirth.Append(cnp[5]).Append(cnp[6]);

            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dt;
            if(!DateTime.TryParse(string.Format("{0}-{1}-{2}", yearOfBirth.ToString(), monthOfBirth.ToString(), dayOfBirth.ToString()), out dt))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "CNP-ul este invalid!"));
            }
            DateTime birthDate = new DateTime(int.Parse(yearOfBirth.ToString()), int.Parse(monthOfBirth.ToString()), int.Parse(dayOfBirth.ToString()));
            TimeSpan time = currentDate-birthDate;
            int years = (int)time.TotalDays / 365;
            return years;
        }

        public override Task<PersonData> GetPersonData(Person request, ServerCallContext context)
        {
            VerifyRequestName(request.Name);
            Console.WriteLine("Nume client: " + request.Name + " | CNP client:" + request.Cnp);

            return Task.FromResult(new PersonData
            {
                Age = GetPersonAge(request.Cnp),
                Gender = GetPersonGender(request.Cnp)
            });
        }
    }
}
